#!/bin/bash

if [ $# -ne 1 ]; then
        echo "${0} student_dir"
        echo "    Example: ${0} ../../YOURDIRECTORY"
        exit 1
fi
student_dir="${1}"; shift

THIS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ASSIGNMENT_DIR="${student_dir}/assignment3"
POINTS=0
MAX_POINTS=0
RESULT_TXT="RESULT.txt"

# ----------
# Functions.

function assert() {
        local actual="${1}"; shift
        local expected="${1}"; shift

        if [ "${expected}" == "-" ]; then
                if [ "${actual}" = "-" -o "${actual}" = " " -o "${actual}" = "" -o "${actual}" = "0" ]; then
                        POINTS=$(( ${POINTS} + 1 ))
                else
                        echo "Incorrect ${ex_name}: expected <${expected}> was <${actual}>"
                fi
        elif [ "${actual}" == "${expected}" ]; then
                POINTS=$(( ${POINTS} + 1 ))
        else
                echo "Incorrect ${ex_name}: expected <${expected}> was <${actual}>"
        fi
}

function exe() {
        local ex_name="${1}"; shift
        local expected_npe="${1}"; shift
        local expected_verb="${1}"; shift

        local ex_dir="${ASSIGNMENT_DIR}/${ex_name}"

        printf "Running: ${ex_name}\n"

        # Count the number of examples/max points.
        MAX_POINTS=$(( ${MAX_POINTS} + 2 ))

        # Clean student dir.
        ( cd ${ASSIGNMENT_DIR}; git clean -xdf )

        # Copy example directory to student dir.
        #cp -r ${ex_name} ${ex_dir}

        # Remove all files with results.
        for f in $(find ${ASSIGNMENT_DIR} -name "${RESULT_TXT}"); do
                rm -rf ${f}
        done

        # Run student code.
        ( cd ${ASSIGNMENT_DIR}
                ./s.sh "https://gitlab.com/ming.sun/${ex_name}.git" > log.txt
        )

        # Make sure that the result file exist (only one).
        has_result_file=$(find ${ASSIGNMENT_DIR} -name "${RESULT_TXT}" | wc -l)
        if [ ${has_result_file} -eq 1 ]; then
                local result_file=$(find ${ASSIGNMENT_DIR} -name "${RESULT_TXT}")
                local actual_npe=$(sed '1q;d' ${result_file})
                local actual_verbs=$(sed '2q;d' ${result_file})

                assert "${actual_npe}" "${expected_npe}"
                assert "${actual_verbs}" "${expected_verb}"
        else
                echo "ERROR: There are ${has_result_file} resulting files and we expect one."
        fi
}

# ----------
# Main.

# Check if convensions are respected and run example if everything
# seems OK.
if [ ! -d "${ASSIGNMENT_DIR}" ]; then
        echo "ERR: Directory ${ASSIGNMENT_DIR} does not exist."
elif [ ! -x "${ASSIGNMENT_DIR}/s.sh" ]; then
        echo "ERR: File ${ASSIGNMENT_DIR}/s.sh is not available or executable."
else
        exe "ex1" "2" "-"
        exe "ex2" "1" "5"
        exe "ex3" "-" "-"
        exe "ex4" "-" "-"
        exe "ex5" "1" "1"
        exe "ex6" "2" "4"
        exe "ex7" "1" "6"
        exe "ex8" "-" "-"
        exe "ex9" "-" "-"
        #exe "ex10" "18" "28329" "https://github.com/apache/commons-net"
        #( cd ${ASSIGNMENT_DIR}
        #        ./s.sh "https://github.com/apache/commons-net" > log.txt
        #)

fi

# Print number of points.
echo "Points: ${POINTS} / ${MAX_POINTS}"
