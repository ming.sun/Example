#!/bin/bash

THIS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
MINI="MINI.java"

# 1. Check if MINI.java is there; we also consider this BAD, although
# we could have picked another value.
if [ ! -e ${THIS}/${MINI} ]; then
        echo "BAD"
        exit 111
fi

# 2. Compile MINI.java
javac ${MINI} &> /dev/null

# 3. Check if compilation was successful.
if [ $? -ne 0 ]; then
        echo "BAD"
        exit 0
fi

# 4. Check if we have all lines that we need for CRASH.
has_one=$(cat ${MINI} | grep 'i++;' | wc -l)

if [ ${has_one} -eq 1 ]; then
        # We have all the lines.
        echo "CRASH"
else
        # Some lines are removed.
        echo "GOOD"
fi
