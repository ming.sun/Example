public class MINI{
    // CRASH : can be compiled but crash.
    public static void main(String[] a){
        Integer i = 42;
        Integer j = null;
        Integer k = 43;
 
        add(i, j);
        // add(i, k); // Removed
    }
   
    public static Integer add(Integer i, Integer j){
        return i + j;
    };
}
