#!/bin/bash

if [ $# -ne 1 ]; then
        echo "${0} student_dir"
        echo "    Example: ${0} ../../YOURDIRECTORY"
        exit 1
fi
student_dir="${1}"; shift

THIS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ASSIGNMENT_DIR="${student_dir}/assignment2"
POINTS=0
MAX_POINTS=0
MINI_JAVA="MINI.java"
MINI_CLASS="MINI.class"

# ----------
# Functions.

function clean.this() {
        ### Clean this.
        local ex_name="${1}"; shift

        ( cd ${THIS}/${ex_name}
                rm -f ${MINI_JAVA}
                rm -f *.class
        )
}

function exe() {
        local ex_name="${1}"; shift
        local ex_dir="${ASSIGNMENT_DIR}/${ex_name}"

        printf "Running: ${ex_name}\n"

        # Count the number of examples/max points.
        MAX_POINTS=$(( ${MAX_POINTS} + 1 ))

        clean.this "${ex_name}"
        # Get expected value.
        ( cd ${THIS}/${ex_name}
                cp RESULT ${MINI_JAVA}
                javac -g:none ${MINI_JAVA}
        )
        expected=$(md5sum ${THIS}/${ex_name}/${MINI_CLASS} | cut -f1 -d' ')
        clean.this "${ex_name}"

        # Clean student dir.
        rm -rf ${ex_dir}
        # Copy example directory to student dir.
        cp -r ${ex_name} ${ex_dir}

        # Run student code.
        ( cd ${ASSIGNMENT_DIR}
                ./s.sh ${ex_name} > log.txt
        )

        # Check the final result.
        if [ -e ${ex_dir}/${MINI_JAVA} ]; then
                ( cd ${ex_dir}; javac -g:none ${MINI_JAVA} )
                actual=$(md5sum ${ex_dir}/${MINI_CLASS} | cut -f1 -d' ')
        fi
        if [ "${actual}" = "${expected}" ]; then
                POINTS=$(( ${POINTS} + 1 ))
        else
                echo "Incorrect ${ex_name}: expected <${expected}> was <${actual}>"
        fi
}

# ----------
# Main.

# Check if convensions are respected and run example if everything
# seems OK.
if [ ! -d "${ASSIGNMENT_DIR}" ]; then
        echo "ERR: Directory ${ASSIGNMENT_DIR} does not exist."
elif [ ! -x "${ASSIGNMENT_DIR}/s.sh" ]; then
        echo "ERR: File ${ASSIGNMENT_DIR}/s.sh is not available or executable."
else
        exe "ex1"
        exe "ex2"
        exe "ex3"
        exe "ex4"
        exe "ex5"
        exe "ex6"
        exe "ex7"
        exe "ex8"
        exe "ex9"
        exe "ex10"        
fi

# Print number of points.
echo "Points: ${POINTS} / ${MAX_POINTS}"
