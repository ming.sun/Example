public class INPUT {
    String m(int y) {
        int i = 0;
        for (; i < y; i++) {
            if (i == 10) {
                break;
            }
            if (i < i * i - 5) {
                i *= 2;
                continue;  // Line for crash
            }
        }
        if (i > 5)
            return "Good";
        return "Great";
    }
}