public class INPUT {
    int m(int x) {
        int y = x * x;
        switch(x) {
            case 1:
                System.out.println(x + 1);
                y = y * 2;
                break;
            case 2:
                System.err.println(x - 2);
                y = y % 2;
                break;
            case 3:
                y++;  // Line for crash
                break;
            default:
                return 13;
        }
        return y;
    }
}