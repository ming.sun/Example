public class INPUT{
    // This program will give you NullPointerException.
    public static void main(String[] a){
        Integer i = 42;
        Integer j = null;
        Integer k = 43;
 
        add(i, j);
        add(i, k);
    }
   
    public static Integer add(Integer i, Integer j){
        return i + j;
    };
}
