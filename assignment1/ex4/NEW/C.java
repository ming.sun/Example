
class A {
    public int m() {
        // This will be modified.
        return 22;
    }
}

public class C extends A {
    public int a() {
        return m();
    }

    public int b() {
        return 88;
    }
}
