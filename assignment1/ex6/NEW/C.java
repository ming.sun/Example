
public class C {
    public String m(int a, int b) {
        if (a > b) {
            throw new RuntimeException();
        }

        // the change will be here
        return "value2";
    }

    public void n(int a, int b) {
        m(a, b);
    }

    public void a() {}
}
