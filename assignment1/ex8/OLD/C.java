
public class C {
    public int m(int a, int b) {
        // this one is changed
        return a + b;
    }

    public int m(int a) {
        // this one is used
        return a;
    }

    public int n() {
        return 10;
    }
}
