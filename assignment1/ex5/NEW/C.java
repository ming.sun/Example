
public class C {
    public void m(int a) {
        class A {
            public int m(int a) {
                // this will change
                return 3 + 10 + a;
            }
        }

        if (a > 10) {
            new A().m(a);
        } else {
            //System.out.println("");
        }
    }
}
